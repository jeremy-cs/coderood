<?php
/**
 * Plugin Name:       Linksmith Functionality
 * Plugin URI:        https://www.linksmith.nl/
 * Description:       Functionality for the website
 * Version:           1.0.0
 * Author:            Linksmith
 * Author URI:        https://www.linksmith.nl/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       linksmith
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( !defined( 'WPINC' ) ) {
	die;
}


if( !class_exists( 'Linksmith' ) ) {
	class Linksmith {
		/**
		 * Instance of the class
		 *
		 * @since 1.0.0
		 * @var Instance of Linksmith class
		 */
		private static $instance;
		/**
		 * Instance of the plugin
		 *
		 * @since 1.0.0
		 * @static
		 * @staticvar array $instance
		 * @return Instance
		 */
		public static function instance() {
			if ( !isset( self::$instance ) && ! ( self::$instance instanceof Linksmith ) ) {
				self::$instance = new Linksmith;
				self::$instance->define_constants();
				add_action( 'plugins_loaded', array( self::$instance, 'load_textdomain' ) );
				self::$instance->includes();
				self::$instance->init = new Linksmith_Init();
			}
			return self::$instance;
		}

		/**
		 * Define the plugin constants
		 *
		 * @since  1.0.0
		 * @access private
		 * @return void
		 */
		private function define_constants() {
			// Plugin Version
			if ( ! defined( 'LINKSMITH_VERSION' ) ) {
				define( 'LINKSMITH_VERSION', '1.0.0' );
			}
			// Prefix
			if ( ! defined( 'LINKSMITH_PREFIX' ) ) {
				define( 'LINKSMITH_PREFIX', 'linksmith_' );
			}
			// Textdomain
			if ( ! defined( 'LINKSMITH_TEXTDOMAIN' ) ) {
				define( 'LINKSMITH_TEXTDOMAIN', 'linksmith' );
			}
			// Plugin Options
			if ( ! defined( 'LINKSMITH_OPTIONS' ) ) {
				define( 'LINKSMITH_OPTIONS', 'linksmith-options' );
			}
			// Plugin Directory
			if ( ! defined( 'LINKSMITH_PLUGIN_DIR' ) ) {
				define( 'LINKSMITH_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
			}
			// Plugin URL
			if ( ! defined( 'LINKSMITH_PLUGIN_URL' ) ) {
				define( 'LINKSMITH_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
			}
			// Plugin Root File
			if ( ! defined( 'LINKSMITH_PLUGIN_FILE' ) ) {
				define( 'LINKSMITH_PLUGIN_FILE', __FILE__ );
			}
		}
		/**
		 * Load the required files
		 *
		 * @since  1.0.0
		 * @access private
		 * @return void
		 */
		private function includes() {
			$includes_path = plugin_dir_path( __FILE__ ) . 'linksmith/includes/';

			require_once $includes_path . 'class-linksmith-register-post-types.php';
			require_once $includes_path . 'class-linksmith-register-taxonomies.php';
			require_once $includes_path . 'class-linksmith-acf.php';
			require_once $includes_path . 'class-linksmith-blocks.php';

			require_once $includes_path . 'class-linksmith-init.php';
		}

		/**
		 * Load the plugin text domain for translation.
		 *
		 * @since  1.0.0
		 * @access public
		 */
		public function load_textdomain() {
			$linksmith_lang_dir = dirname( plugin_basename( LINKSMITH_PLUGIN_FILE ) ) . '/languages/';
			$linksmith_lang_dir = apply_filters( 'LINKSMITH_lang_dir', $linksmith_lang_dir );
			$locale = apply_filters( 'plugin_locale',  get_locale(), LINKSMITH_TEXTDOMAIN );
			$mofile = sprintf( '%1$s-%2$s.mo', LINKSMITH_TEXTDOMAIN, $locale );
			$mofile_local  = $linksmith_lang_dir . $mofile;
			$mofile_global = WP_LANG_DIR . '/edd/' . $mofile;
			if ( file_exists( $mofile_local ) ) {
				load_textdomain( LINKSMITH_TEXTDOMAIN, $mofile_local );
			} else {
				load_plugin_textdomain( LINKSMITH_TEXTDOMAIN, false, $linksmith_lang_dir );
			}
		}

		/**
		 * Throw error on object clone
		 *
		 * @since  1.0.0
		 * @access public
		 * @return void
		 */
		public function __clone() {
			_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', LINKSMITH_TEXTDOMAIN ), '1.6' );
		}
		/**
		 * Disable unserializing of the class
		 *
		 * @since  1.0.0
		 * @access public
		 * @return void
		 */

		public function __wakeup() {
			_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', LINKSMITH_TEXTDOMAIN ), '1.6' );
		}
	}
}
/**
 * Return the instance
 *
 * @since 1.0.0
 * @return object The Safety Links instance
 */
function Linksmith_Run() {
	return Linksmith::instance();
}
Linksmith_Run();

