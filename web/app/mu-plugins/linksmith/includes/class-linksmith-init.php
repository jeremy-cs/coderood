<?php
/**
 * Main Init Class
 *
 * @package     Linksmith
 * @subpackage  Linksmith/includes
 * @copyright   Copyright (c) 2018, Jeremy Crowlesmith
 * @since       1.0.0
 */
class Linksmith_Init {
	/**
	 * Initialize the class
	 */
	public function __construct() {
		$register_post_types     = new Linksmith_Register_Post_Types();
		$register_taxonomies     = new Linksmith_Register_Taxonomies();
		$register_acf            = new Linksmith_ACF();
		$register_blocks         = new Linksmith_Blocks();
	}
}