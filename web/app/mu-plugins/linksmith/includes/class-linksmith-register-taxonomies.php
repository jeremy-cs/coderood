<?php
/**
 * Register Custom Taxonomies
 *
 * @package     Linksmith
 * @subpackage  Linksmith/includes
 * @copyright   Copyright (c) 2018, Jeremy Crowlesmith
 * @since       1.0.0
 */

class Linksmith_Register_Taxonomies {

	/**
	 * Initialize the class
	 */
	public function __construct() {
//		add_action( 'init', array( $this, 'register_resource_category_taxonomy' ), 10 );
//		add_action( 'init', array( $this, 'register_workshop_town_taxonomy' ) );
//		add_action( 'init', array( $this, 'register_production_status_taxonomy' ) );
//		add_action( 'init', array( $this, 'register_performer_status_taxonomy' ) );
	}

	/**
	 * Register Resource Category Taxonomy
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function register_resource_category_taxonomy() {
		register_extended_taxonomy('resource-category', 'resource', [
			'meta_box'           => 'radio',
			'public'             => true,
			'publicly_queryable' => true,
			'hierarchical'       => true,
			'show_ui'            => true,
			'has_archive'        => true,
			'show_admin_column'  => true,
			'query_var'          => true,
			'show_in_nav_menus'  => true,
		], [
			'singular'  => __( 'Resource Categorie', LINKSMITH_TEXTDOMAIN ),
			'plural'    => __( 'Resource Categoriën', LINKSMITH_TEXTDOMAIN ),
			'slug'      => 'resources'
		]);
	}

	/**
	 * Register Workshop Town Taxonomy
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function register_workshop_town_taxonomy() {
		register_extended_taxonomy('workshop_town', 'workshop', [
		], [
			'singular' => 'Stad',
			'plural' => 'Steden'
		]);
	}

	/**
	 * Register Production Status Taxonomy
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function register_production_status_taxonomy() {
		register_extended_taxonomy('production_status', 'production', [
			'meta_box' => 'radio',
			'allow_hierarchy' => false
		], [
			'singular' => 'Status',
			'plural' => 'Status'
		]);
	}

	/**
	 * Register Performer Status Taxonomy
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function register_performer_status_taxonomy() {
		register_extended_taxonomy('performer_status', 'performer', [
			'meta_box' => 'radio',
			'allow_hierarchy' => false
		], [
			'singular' => 'Status',
			'plural' => 'Status'
		]);
	}
}