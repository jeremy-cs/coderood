<?php

/**
 * Register Custom Blocks
 *
 * @package     Linksmith
 * @subpackage  Linksmith/includes
 * @copyright   Copyright (c) 2018, Jeremy Crowlesmith
 * @since       1.0.0
 */
class Linksmith_Blocks {
	/**
	 * Initialize the class
	 */
	public function __construct() {
		add_action( 'after_setup_theme', array( $this, 'disable_custom_colors' ) );
		add_action( 'after_setup_theme', array( $this, 'gutenberg_color_palette') );
		add_action( 'enqueue_block_editor_assets', array( $this, 'gutenberg_scripts') );
	}

	/**
	 * Disable custom color in Blocks
	 */
	function disable_custom_colors() {
		add_theme_support( 'disable-custom-colors' );
	}

	/**
	 * Custom colors for use in the editor.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/reference/theme-support/
	 */
	function gutenberg_color_palette() {
		add_theme_support(
			'editor-color-palette', [
				[
					'name'  => esc_html__( 'White', '@@textdomain' ),
					'slug'  => 'white',
					'color' => esc_html( get_theme_mod( 'white', '#ffffff' ) ),
				],
				[
					'name'  => esc_html__( 'Black', '@@textdomain' ),
					'slug'  => 'black',
					'color' => esc_html( get_theme_mod( 'black', '#000000' ) ),
				],
				[
					'name'  => esc_html__( 'Yellow', '@@textdomain' ),
					'slug'  => 'yellow',
					'color' => esc_html( get_theme_mod( 'yellow', 'rgba(255, 213, 0, 1)' ) ),
				],
				[
					'name'  => esc_html__( 'Blue', '@@textdomain' ),
					'slug'  => 'blue',
					'color' => esc_html( get_theme_mod( 'blue', 'rgba(0, 209, 255, 1' ) ),
				],
				[
					'name'  => esc_html__( 'Red', '@@textdomain' ),
					'slug'  => 'red',
					'color' => esc_html( get_theme_mod( 'red', 'rgba(166, 0, 0, 1)' ) ),
				],
			]
		);
	}

	/**
	 * Gutenberg scripts and styles
	 * @see https://www.billerickson.net/wordpress-color-palette-button-styling-gutenberg
	 */
	function gutenberg_scripts() {
		wp_enqueue_script(
			'sage/editor.js',
			\App\asset_path('scripts/editor.js'), [
				'wp-i18n', 'wp-element', 'wp-blocks', 'wp-components', 'wp-editor', 'wp-dom'
			], null, true
		);
	}
}
