<?php
/**
 * Register custom post types
 *
 * @package     Linksmith
 * @subpackage  Linksmith/includes
 * @copyright   Copyright (c) 2018, Jeremy Crowlesmith
 * @since       1.0.0
 */
class Linksmith_ACF {
	/**
	 * Initialize the class
	 */
	public function __construct() {
		add_action( 'init', array( $this, 'add_acf_options_pages' ) );
		add_action( 'admin_menu', array( $this, 'hide_acf_menu_items_from_admin_menu' ), PHP_INT_MAX );

//		add_action( 'acf/init', array( $this, 'register_acf_blocks' ) );
//		add_action( 'enqueue_block_editor_assets', array( $this, 'enqueue_block_editor_styles' ) );

//		add_action( 'enqueue_block_editor_assets', array( $this, 'enqueue_block_editor_calltoaction_scripts' ) );
//		add_action( 'enqueue_block_assets', array( $this, 'enqueue_block_calltoaction_styles' ) );
//
//		add_action( 'enqueue_block_assets', array( $this, 'enqueue_block_latestposts_styles' ) );
//
//		add_action( 'enqueue_block_assets', array( $this, 'enqueue_block_testimonial_styles' ) );

		add_filter( 'acf/settings/save_json', array( $this, 'place_acf_save_json_in_directory' ), PHP_INT_MAX, 1 );
		add_filter( 'acf/settings/load_json', array( $this, 'place_acf_load_json_from_directory' ), PHP_INT_MAX, 1 );
	}

	/**
	 * Place ACF JSON in field-groups directory
	 */
	function place_acf_save_json_in_directory($path) {
		return LINKSMITH_PLUGIN_DIR . 'linksmith/field-groups';
	}

	/**
	 * Place ACF JSON in field-groups directory
	 */
	function place_acf_load_json_from_directory($paths) {
		unset($paths[0]);
		$paths[] = LINKSMITH_PLUGIN_DIR . 'linksmith/field-groups';
		return $paths;
	}

	/**
	 * Hide menu items from the admin menu
	 */
	function hide_acf_menu_items_from_admin_menu() {
		// List of users that don't have pages removed
		$admins = [
			'linksmith'
		];

		$current_user = wp_get_current_user();

		if (!in_array($current_user->user_login, $admins)) {
			remove_menu_page('edit.php?post_type=acf-field-group');
		}
	}

	/**
	 * Add ACF options pages
	 */
	function add_acf_options_pages() {
		if (!function_exists('acf_add_options_page')) {
			return;
		}
		acf_add_options_page([
			'page_title'    => 'Code Rood Settings',
			'menu_title'    => 'Code Rood Settings',
			'menu_slug'     => 'cr-settings',
			'capability'    => 'edit_posts',
			'parent_slug'   => '',
			'position'      => 2, // Below 'Dashboard' menu item
			'icon_url'      => 'dashicons-admin-generic',
			'redirect'		=> false
		]);

//		acf_add_options_sub_page([
//			'page_title'    => 'Footer',
//			'menu_title'    => 'Footer',
//			'menu_slug'     => 'footer',
//			'capability'    => 'manage_options',
//			'parent_slug'   => 'ghd-settings'
//		]);
	}

	// Register a testimonial ACF Block
	function register_acf_blocks( ) {

		if( ! function_exists('acf_register_block') )
			return;
//
		// register a CTA block
		acf_register_block( array(
			'name'              => 'slider',
			'title'             => __( 'Slider' ),
			'description'       => __( 'Slider element.' ),
			'category'          => 'formatting',
			'mode'			    => 'edit',
			'align'             => 'full',
			'supports'          => array( 'align' => false, ),
			'render_callback'   => array($this, 'acf_block_render_callback'),
			'icon'	            => 'image',
			'keywords'          => array('slider'),
		) );
//
//		// register a Latests posts block
//		acf_register_block( array(
//			'name'              => 'latestposts',
//			'title'             => __( 'Latest posts' ),
//			'description'       => __( 'Latest posts element for grid section.' ),
//			'category'          => 'formatting',
//			'mode'			    => 'edit',
//			'align'             => 'full',
//			'supports'          => array( 'align' => false, ),
//			'render_callback'   => array($this, 'acf_block_render_callback'),
//			'icon'			    => 'admin-post',
//			'keywords'		    => array('latest'),
//		) );
//
//		// register a Latests posts block
//		acf_register_block( array(
//			'name'              => 'testimonial',
//			'title'             => __( 'Testimonial' ),
//			'description'       => __( 'Testimonial for inline content.' ),
//			'category'          => 'formatting',
//			'mode'			    => 'edit',
//			'align'             => 'full',
//			'supports'          => array( 'align' => false, ),
//			'render_callback'   => array($this, 'acf_block_render_callback'),
//			'icon'			    => 'format-quote',
//			'keywords'		    => array('testimonial'),
//		) );
	}

	function acf_block_render_callback($block) {
		$name = str_replace('acf/', '', $block['name']);
		$path = get_template_directory() .'/views/partials/block-'. $name .'.php';

		if (file_exists($path)) {
			include($path);
		}
	}

	// Enqueue block editor styles
	static function enqueue_block_editor_styles() {
		$path = \App\asset_path( 'styles/editor.css' );
		wp_enqueue_style( 'editor_styles', 
			$path, 
			array(),
			'1.0'
		);
	}

	// Enqueue Back-end calltoaction block scripts
	static function enqueue_block_editor_calltoaction_scripts() {
		$script_handle = 'calltoaction_script';

		wp_register_script(
			$script_handle,
			\App\asset_path('scripts/calltoaction.js'),
			['acf-input', 'wp-blocks', 'wp-element', 'wp-components', 'wp-i18n' ],
			filemtime( get_theme_file_path( '/assets/scripts/blocks/calltoaction.js' ) )
		);
		wp_enqueue_script( $script_handle );
	}

	// Enqueue Front-end calltoaction block styles
	static function enqueue_block_calltoaction_styles() {
		$path = \App\asset_path('styles/calltoaction.css');
		wp_enqueue_style('calltoaction_styles', $path, array(),
		filemtime( get_theme_file_path( '/assets/scripts/blocks/calltoaction.css' ) ));
	}

	// Enqueue Front-end latestposts block styles
	static function enqueue_block_latestposts_styles() {
		$path = \App\asset_path('styles/latestposts.css');
		wp_enqueue_style('latestposts_styles', $path, array(),
			filemtime( get_theme_file_path( '/assets/scripts/blocks/latestposts.css' ) ));
	}

	// Enqueue Front-end testimonial block styles
	static function enqueue_block_testimonial_styles() {
		$path = \App\asset_path('styles/testimonial.css');
		wp_enqueue_style('testimonial_styles', $path, array(),
			filemtime( get_theme_file_path( '/assets/scripts/blocks/testimonial.css' ) ));
	}
}
