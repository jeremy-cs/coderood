<?php
/**
 * Register custom post types
 *
 * @package     Linksmith
 * @subpackage  Linksmith/includes
 * @copyright   Copyright (c) 2018, Jeremy Crowlesmith
 * @since       1.0.0
 */

class Linksmith_Register_Post_Types {
	/**
	 * Initialize the class
	 */
	public function __construct() {
//		add_action( 'init', array( $this, 'register_resource_post_type' ), 15 );
//		add_filter( 'post_type_link', array( $this,  'add_category_to_url'), 1, 3 );
//		add_action( 'init', array( $this, 'register_testimonial_post_type' ) );
//		add_action( 'init', array( $this, 'register_production_post_type' ) );
//		add_action( 'init', array( $this, 'register_performer_post_type' ) );
	}

	/**
	 * Register Resource Post Type
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function register_resource_post_type() {
		register_extended_post_type('resource',
			[
				'menu_icon'             => 'dashicons-admin-tools',
				'show_in_feed'          => true,
				'show_in_rest'          => true,
				'with_front'            => false,
				# Show all posts on the post type archive:
				'archive'               => [
					'nopaging' => true
				],
				'has_archive'           => 'resources',
				'hierarchical'          => false,
				'supports'              => [
					'title', 'editor', 'page-attributes'
				],
				# Add some custom columns to the admin screen:
				'admin_cols' => [
					'resource-category' => [
						'title'    => 'Categorie',
						'taxonomy' => 'resource-category'
					],
				],
			],
			[
				'singular'              => __( 'Resource', LINKSMITH_TEXTDOMAIN ),
				'plural'                => __( 'Resources', LINKSMITH_TEXTDOMAIN ),
				'slug'                  => 'resources/%category%',
			]
		);
	}

	// to add resource custom taxonomy to resource links - for later
	function add_category_to_url( $post_link, $id = 0 ){
		$post = get_post($id);
		if ( is_object( $post ) ){
			$terms = wp_get_object_terms( $post->ID, 'resource-category' );
			if( $terms ){
				return str_replace( '%category%' , $terms[0]->slug , $post_link );
			}
			if(!$terms && ($post->post_type == 'resource')) {
				return str_replace( '%category%/' , '' , $post_link );
			}
		}
		return $post_link;
	}


	/**
	 * Register Production Post Type
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function register_production_post_type() {
		register_extended_post_type('production',
			[
				'menu_icon'             => 'dashicons-visibility',
				# Add the post type to the site's main RSS feed:
				'show_in_feed'          => true,
				# Show all posts on the post type archive:
				'archive'               => [
					'nopaging' => true
				],
				'hierarchical'          => false,
				'supports'              => [
					'title', 'thumbnail', 'editor', 'excerpt', 'revisions', 'page-attributes'
				],
				'admin_cols' => array(
					// The default Title column:
					'title',
					// A taxonomy terms column:
					'production_status' => array(
						'taxonomy' => 'production_status'
					),
					// A featured image column:
					'featured_image' => array(
						'title'          => 'Afbeelding',
						'featured_image' => 'thumbnail'
					),
					'menu_order' => [
						'title'    => 'Order',
						'post_field' => 'menu_order',
					],
				),
			],
			[
				'singular'              => __( 'Productie', LINKSMITH_TEXTDOMAIN ),
				'plural'                => __( 'Producties', LINKSMITH_TEXTDOMAIN ),
				'slug'                  => 'production',
			]
		);
	}

	/**
	 * Register Performer Post Type
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function register_performer_post_type() {
		register_extended_post_type('performer',
			[
				'menu_icon'             => 'dashicons-admin-users',
				# Add the post type to the site's main RSS feed:
				'show_in_feed'          => true,
				# Show all posts on the post type archive:
				'archive'               => [
					'nopaging' => true
				],
				'hierarchical'          => false,
				'supports'              => [
					'title', 'thumbnail', 'editor', 'excerpt', 'revisions', 'page-attributes'
				],
				# Add some custom columns to the admin screen:
				'admin_cols' => [
					'workshop__featured_image' => [
						'title'          => 'Photo',
						'featured_image' => 'thumbnail'
					],
					'performer_status' => [
						'title'    => 'Status',
						'taxonomy' => 'performer_status'
					],
					'menu_order' => [
						'title'    => 'Order',
						'post_field' => 'menu_order',
					],
				],
			],
			[
				'singular'              => __( 'Performer', LINKSMITH_TEXTDOMAIN ),
				'plural'                => __( 'Performers', LINKSMITH_TEXTDOMAIN ),
				'slug'                  => 'performer',
			]
		);
	}
}
