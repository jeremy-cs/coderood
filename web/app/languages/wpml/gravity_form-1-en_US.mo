��          �       �      �  `   �          $  R  0  
   �  !   �  .   �     �     �  	   �     �  B   	     P	     _	     u	     �	     �	  /   �	  .   �	     �	  
   
     
  &   
  (   F
      o
  l   p
     �
     �
  d  �
  
   _      j  &   �     �     �     �     �  &   �  
                        "  l   '  d  �     �                      3   Bedankt voor je bericht! We sturen je zo een berichtje met wat aanvullende informatie en vragen. Doe mee met Code Rood E-mailadres Hoi {Naam/Alias:10}!

<span class="im">Dankjewel voor je aanmelding en wat goed dat je actief wilt worden bij </span>Code Rood. We hebben je nodig! Alleen samen kunnen we de<span class="im"> aandeelhoudersvergadering van Shell in 2020 verstoren, blokkeren en cancellen. Of je je nu een paar uur per maand of een dag per week kunt inzetten, iedereen is welkom! </span>

Je kunt overwegen om een account te maken op de <a href="https://wiki.code-rood.org/knowledgebase/intro-to-chat-code-rood-org/">chat server,</a> ga in het kanaal van de werkgroep waar je in geinteresseerd bent en stel je voor!
<ul>
 	<li>De Mobilisatie werkgroep zit in het kanaal <strong>#mobi-public</strong>. Mobi probeert zo veel mogelijk mensen bij Shell Must Fall te betrekken. We zijn op zoek naar mensen die graag met andere mensen praten over de campagne, of juist achter de schermen helpen met het organiseren van actietrainingen.</li>
 	<li>De Media &amp; communicatie werkgroep zit in het kanaal <strong>#media-public</strong>. Mecom houdt zich bezig met pers, woordvoering, social media, teksten schrijven voor de website en het regelen foto- en videomateriaal.</li>
</ul>
Voor kleine updates over Shell Must Fall!, ga in dit Telegram broadcast channel: <a href="https://t.me/ShellMustFall">https://t.me/ShellMustFall</a>

Groetjes,
Code Rood

&nbsp;

<a href="https://code-rood.org/doneer/">Steun ons financieel</a>
Volg ons op <a href="https://twitter.com/klimaatactie">Twitter</a> / <a href="https://www.facebook.com/CodeRood.org/">Facebook</a> / <a href="https://www.youtube.com/c/CODEROOD-Klimaatactie">YouTube</a> Naam/Alias Nieuwe inzending van {form_title} Schrijf me ook in voor de codeROOD nieuwsbrief Stad Telefoon Versturen Welkom bij Code Rood! checkbox-7-choice-0-Schrijf-me-ook-in-voor-de-codeROOD-nieuwsbrief field-10-label field-11-defaultValue field-3-label field-4-label field-9-label field-confirmation-message_Standaardbevestiging field-notification-message_Mail-naar-aanmelder form_submit_button form_title nl notification-subject_Beheerdersmelding notification-subject_Mail-naar-aanmelder  Thank you for your submission! We will send an email shortly with additional information and some questions. Join Code Rood Email address Hi {Naam/Alias:10}! 

Thank you for signing up and great that you would like to get involved with Code Rood (Code Red).

Any helping hand is needed because only together we will be able to disrupt, bloc and cancel the Shell AGM in 2020.

Any contribution to this project is welcome. Whether it is through helping out one hour per month or a full day a week. There is a wide range of tasks and roles to be distributed, so surely we will also have the perfect task for you!

If you already know what <a href="https://code-rood.org/en/working-groups/">working group </a> you would like to join please let us know so that we can bring you in touch with the right contact person.

If you prefer first getting in touch with a Code Rood member to explain you a bit better what the options are to get involved then that is also possible.

In any case please send us a mail with the following information:
- Where do you live?
- How much time do you have?
- Are you already interest in joining a specific <a href="https://code-rood.org/en/working-groups/">working group </a>

Cheers,
Code Rood

<a href="https://code-rood.org/en/donate/">Support us financially</a>
Volg ons op <a href="https://twitter.com/klimaatactie">Twitter</a> / <a href="https://www.facebook.com/CodeRood.org/">Facebook</a> / <a href="https://www.youtube.com/c/CODEROOD-Klimaatactie">YouTube</a>  Name/alias New submission from {form_title} Also sign me up to codeROOD newsletter City Phone Submit Welcome to codeROOD! Also sign me up to codeROOD newsletter Name/alias en Email address Phone City Thank you for your submission! We will send an email shortly with additional information and some questions. Hi {Naam/Alias:10}! 

Thank you for signing up and great that you would like to get involved with Code Rood (Code Red).

Any helping hand is needed because only together we will be able to disrupt, bloc and cancel the Shell AGM in 2020.

Any contribution to this project is welcome. Whether it is through helping out one hour per month or a full day a week. There is a wide range of tasks and roles to be distributed, so surely we will also have the perfect task for you!

If you already know what <a href="https://code-rood.org/en/working-groups/">working group </a> you would like to join please let us know so that we can bring you in touch with the right contact person.

If you prefer first getting in touch with a Code Rood member to explain you a bit better what the options are to get involved then that is also possible.

In any case please send us a mail with the following information:
- Where do you live?
- How much time do you have?
- Are you already interest in joining a specific <a href="https://code-rood.org/en/working-groups/">working group </a>

Cheers,
Code Rood

<a href="https://code-rood.org/en/donate/">Support us financially</a>
Volg ons op <a href="https://twitter.com/klimaatactie">Twitter</a> / <a href="https://www.facebook.com/CodeRood.org/">Facebook</a> / <a href="https://www.youtube.com/c/CODEROOD-Klimaatactie">YouTube</a>  Submit Join Code Rood en New submission from {form_title} Welcome to codeROOD! 