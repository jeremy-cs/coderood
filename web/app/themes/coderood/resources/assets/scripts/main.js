// import external dependencies
import 'jquery';

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import shellMustFall from './routes/shellMustFall';

// import then needed Font Awesome functionality
import { library, dom } from '@fortawesome/fontawesome-svg-core';
// import the Facebook and Twitter icons
import { faChevronDown, faChevronUp } from '@fortawesome/free-solid-svg-icons';

// add the imported icons to the library
library.add(faChevronDown, faChevronUp);

// tell FontAwesome to watch the DOM and add the SVGs when it detects icon markup
dom.watch();

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // Shell Must Fall page
  shellMustFall,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
