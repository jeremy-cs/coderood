
class Editor {

  init() {
    Editor.addColorPickerPalettes();
  }

  static addColorPickerPalettes() {
    // eslint-disable-next-line
    acf.add_filter('color_picker_args', function( args, field ) {
      args.palettes = ['#4747e6', '#ff3224', '#ffffff', '#000000'];
      return args;
    });
  }
}

(function() {
  const editor = new Editor();
  editor.init();
})(jQuery);