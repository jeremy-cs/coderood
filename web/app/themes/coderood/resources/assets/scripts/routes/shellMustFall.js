import Headroom from 'headroom.js';
import 'slick-carousel/slick/slick.min.js';
import SmoothScroll from 'smooth-scroll/dist/smooth-scroll.polyfills.min.js';
import anime from 'animejs';
// eslint-disable-next-line
import parallax from 'jquery-parallax.js';

export default {
  init() {
    scrolling();
    animateLogo();
    initParalax();
    banner();

    function banner() {
      let banner = document.querySelector('.banner');
      let headroom = new Headroom(banner);
      headroom.init({
        tolerance: 50,
      });
    }

    function initParalax() {
      // let parallaxImages = document.querySelectorAll('.parallax');
      //
      // parallaxImages.forEach( (parallaxImage ) => {
      //   let parallaxWindow = document.createElement('div');
      //   parallaxWindow.classList.add('parallax-window');
      //
      //   let parallaxSlider = document.createElement('div');
      //   parallaxSlider.classList.add('parallax-slider');
      //
      //   parallaxImage.parentNode.insertBefore(parallaxWindow, parallaxImage);
      //   parallaxWindow.appendChild(parallaxSlider);
      //   parallaxSlider.appendChild(parallaxImage);
      // });
      //
      // $('.parallax-window').parallax({
      //   naturalHeight: 400,
      // });

      $('.parallax-window').parallax();
    }

    function scrolling() {
      // eslint-disable-next-line
      var scroll = new SmoothScroll('a[href*="#"]');
    }

    function animateLogo() {
      let footerLogo = document.querySelector('.footer__logo-image');

      var logoSwirl = anime({
        targets: '.footer__logo-image',
        autoplay: false,
        loop: false,
        rotate: {
          value: '-3600',
          duration: 3500,
          easing: 'easeOutCirc',
        },
      });

      footerLogo.addEventListener('click', function() {
        logoSwirl.play();
      });

    }
  },
  finalize() {

  },
};
