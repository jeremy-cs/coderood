import Headroom from 'headroom.js';
import 'slick-carousel/slick/slick.min.js';
import anime from 'animejs';

export default {
  init() {
  },
  finalize() {
    const blacklist = ['shell-must-fall'];
    const bodyClasses = Array.from(document.body.classList);

    if (!bodyClasses.some(bodyClass => blacklist.includes(bodyClass))) {
      console.log('asdg');
      mainMenu();
      sliders();
      animateLogo();
      header();
      // console.log('not on home or single post');
    }

    function header() {
      // grab an element
      var header = document.querySelector('.header');
      // construct an instance of Headroom, passing the element
      var headroom = new Headroom(header);
      // initialise
      headroom.init({
        // vertical offset in px before element is first unpinned
        offset: 0,
        // or scroll tolerance per direction
        tolerance: 40,
        // element which is source of scroll events. Defaults to window
        scroller: header,
        classes: {
          // when element is initialised
          initial: 'headroom',
          // when scrolling up
          pinned: 'headroom--pinned',
          // when scrolling down
          unpinned: 'headroom--unpinned',
        },
      });
    }

    function mainMenu() {
      var $hamburger = $('#hamburger-menu-open-button');
      var $primaryMenu = $('#navbar-primary-menu');

      $primaryMenu.on('show.bs.collapse', function () {
        $hamburger.addClass('is-active');
        $hamburger.attr('aria-expanded', true);
      });

      $primaryMenu.on('hide.bs.collapse', function () {
        $hamburger.removeClass('is-active');
        $hamburger.attr('aria-expanded', false);
      });
    }

    function sliders() {
      $('.slider').slick({});
    }

    function animateLogo() {
      let logo = document.querySelector('.header__brand__link');

      var logoSwirl = anime({
        targets: '#code-rood-logo-swirl',
        autoplay: false,
        loop: false,
        rotate: {
          value: '-3600',
          duration: 2500,
          easing: 'easeOutCirc',
        },
      });

      var logoDrainTimeline = anime.timeline({
        autoplay: false,
        loop: false,
      });

      logoDrainTimeline.add({
        targets: '#code-rood-logo-text',
        translateX: 15,
        translateY: -15,
        translateZ: 15,
        scale: 0.95,
        duration: 50,
        easing: 'easeOutElastic',
      });

      logoDrainTimeline.add({
        targets: '#code-rood-logo-text',
        translateX: 0,
        translateY: 0,
        translateZ: 0,
        scale: 1,
        duration: 50,
        easing: 'easeInElastic',
      });

      logoDrainTimeline.add({
        targets: '#code-rood-logo-swirl',
        rotate: {
          value: '-720',
          duration: 100,
          easing: 'easeOutCirc',
        },
        scale: {
          value: 0,
          duration: 1000,
          easing: 'easeOutCirc',
        },
      }, '-=75');

      logo.addEventListener('mouseenter', function() {
        logoSwirl.play();
      });

      logo.addEventListener('click', function() {
        logoDrainTimeline.play();
      });

      logo.addEventListener('mouseleave', function() {
        // logoAnimated.pause();
      });
    }
  },
};
