<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    @php do_action('get_header') @endphp
    @include('partials.header')
    <div class="wrap container" role="document">

      <div class="cta-banner">
        <?php
          $shell_must_fall_page_id_nl = 7368;
          $id = icl_object_id($shell_must_fall_page_id_nl, 'page', false,ICL_LANGUAGE_CODE);
        ?>
        <a class="cta-banner__link shell-must-fall" href="<?php echo get_permalink($id) ?>" >
          <img src="@asset('images/shell-must-fall-code-rood-2020.png')" />
        </a>
      </div>

      <div class="content">
        <main class="main">
          @yield('content')
        </main>
        @if (App\display_sidebar())
          <aside class="sidebar">
            @include('partials.sidebar')
          </aside>
        @endif
      </div>

      @include('partials.footer-cta')
    </div>
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
</html>
