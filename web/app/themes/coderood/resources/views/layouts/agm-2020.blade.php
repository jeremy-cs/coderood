<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
{{--  <link rel="stylesheet" id="landingpage-css"--}}
{{--        href="@asset('styles/agm2020.css')" type="text/css" media="all">--}}
  <body @php body_class('agm-2020') @endphp>
    <header class="banner">
        <nav class="nav-primary">
          @if (has_nav_menu('landingpage_navigation'))
            {!! wp_nav_menu($landingpagemenu) !!}
          @endif

          {!! $icl_post_languages; !!}
        </nav>
    </header>

    <div class="wrap" role="document">
      <div class="content">
        <main class="main">
          @yield('content')
        </main>
      </div>
    </div>
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
</html>
