{{--
  Template Name: AGM 2020 Template
--}}

@extends('layouts.agm-2020')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-page')
  @endwhile
@endsection
