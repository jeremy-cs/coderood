@extends('layouts.app')

@section('content')

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  <div class="posts-and-events">
    <section class="home__news" >
      <h2>{{ __('Nieuws', 'coderood') }}</h2>

      @while($latest_posts->have_posts()) @php($latest_posts->the_post())
        @include('partials.content-'.get_post_type() )

      <nav class="post-nav">
        <ul class="pager">
          <li class="previous"><?php next_posts_link(__('&larr; Older posts', 'roots')); ?></li>
          <li class="next"><?php previous_posts_link(__('Newer posts &rarr;', 'roots')); ?></li>
        </ul>
      </nav>
      {!! get_the_posts_navigation( [
              'prev_text' => __( '< Oudere berichten', 'coderood' ),
              'next_text' => __( 'Nieuwere berichten >', 'coderood'),
          ])
      !!}
      @endwhile


      @php(wp_reset_postdata())
    </section>

    <section class="home__events" >
      <h2>{{ __('Evenementen', 'coderood') }}</h2>
      {!! do_shortcode( '[tribe_events  tribe-bar="false" view="list"]') !!}

      <section class="home__blog" >
        <h2>{{ __('Blog', 'coderood') }}</h2>
        @while($blog_posts->have_posts()) @php($blog_posts->the_post())
          @include('partials.content-'.get_post_type() )
        @endwhile
      </section>
    </section>


  </div>
@endsection
