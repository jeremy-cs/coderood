{{--
  Template Name: Events
--}}

@extends('layouts.app')

@section('content')
	@include('partials.page-header')
    <div id="tribe-events" >
		<?php tribe_get_view(); ?>
    </div>
@endsection