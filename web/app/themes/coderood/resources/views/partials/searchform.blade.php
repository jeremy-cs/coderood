<form role="search" method="get" class="search-form" action="{{ esc_url( home_url( '/' ) ) }}">

    <div class="form-row">
        <div class="form-group">
            <label for="s" class="sr-only">{{ _x( 'Search for:', 'label' ) }}</label>
            <input type="search" class="form-control search-field" name="s"
               placeholder="{!! esc_attr_x( 'Search &hellip;', 'placeholder' ) !!}"
               value="{{ get_search_query() }}" />
        </div>
    </div>

    <div class="form-row ml-auto pt-4 pr-4">
        <input type="submit" class="btn btn-primary" value="{{ esc_attr_x( 'Search', 'submit button' ) }}" />
    </div>

</form>