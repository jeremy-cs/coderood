<footer class="footer">

  <div class="container">
    <div class="footer__social">
      <ul>
        <li><a href="https://www.facebook.com/klimaatactie" target="_blank">Facebook</a></li>
        <li><a href="https://twitter.com/klimaatactie" target="_blank">@Klimaatactie</a></li>
        <li><a href="https://twitter.com/shellmustfall" target="_blank">@ShellMustFall</a></li>
        <li><a href="http://flickr.com/photos/code-rood" target="_blank">Flickr</a></li>
        <li><a href="https://www.youtube.com/c/CODEROOD-Klimaatactie" target="_blank">YouTube</a></li>
      </ul>
    </div>

    @if (has_nav_menu('footer_navigation'))
      {!! wp_nav_menu($footermenu) !!}
    @endif

    <div class="footer__donate">
      <a href="/doneer" class="footer__donate-button" >Doneer</a>
    </div>

    <div class="footer__logo">
      <img src="@asset('images/CR logo RED.png');"
           class="footer__logo-image"
           alt="Code Rood logo" />
    </div>

  </div>

</footer>
