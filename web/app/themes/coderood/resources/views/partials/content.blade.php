<article @php post_class() @endphp>
  <header>
  <div class="featured-image">
      <?php
          $post = get_post();
          setup_postdata($post);
          $featured_image = get_the_post_thumbnail($post->ID, 'large');
          echo $featured_image;
      ?>
  </div>
    <h2 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>
    @include('partials/entry-meta')
  </header>
  <div class="entry-summary">
    @php the_excerpt() @endphp
  </div>
</article>
