
<div class="footer-cta">

  <h2 class="footer-cta__title">
    {{ __('Steun Code Rood', 'coderood') }}
  </h2>

  <div class="footer-cta__inner" >

    <div class="signup-cta" >
      <h3>
        <a href="{{ get_permalink(1071) }}" class="signup-cta__link" >
          {{ __('Doe mee', 'coderood') }}
        </a>
      </h3>
    </div>

    <div class="doneer-cta" >
      <h3>
        <a href="{{ get_permalink(489) }}" class="doneer-cta__link" >
          {{  __('Doneer', 'coderood') }}
        </a>
      </h3>
    </div>

  </div>

</div>
