<?php

/**
 * This is the template that renders the slider.
 *
 * @param   array $block The block settings and attributes.
 * @param   bool $is_preview True during AJAX preview.
 */

$slider = get_field('image_slider');

// create id attribute for specific styling
$id = 'testimonial-' . $block['id'];

?>

<div id="<?php echo $id; ?>" class="slider" >

    <?php if( have_rows('repeater_field_name') ): ?>

	    <?php while ( have_rows('repeater_field_name') ) : the_row(); ?>

		    <?php the_sub_field('sub_field_name'); ?>

	    <?php endwhile; ?>

	<?php endif; ?>
	<p class="slider__text">
		<?php echo $slider; ?>
	</p>

</div>