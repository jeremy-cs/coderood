<?php

namespace App\Controllers;

use App\wp_bootstrap4_navwalker;
use Sober\Controller\Controller;
use WP_Error;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

	public function topmenu() {
		$args = array(
			'theme_location'    => 'top_navigation',
			'container'         => 'div',
			'container_id'      => 'navbar-top-menu',
//			'container_class'   => 'collapse navbar-collapse',
			'menu_class'        => 'nav ml-auto',
			'depth'             => 2,
			'fallback_cb'       => 'wp_bootstrap4_navwalker::fallback',
			'walker'            => new wp_bootstrap4_navwalker(),
		);
		return $args;
	}

	public function footermenu() {
		$args = array(
			'theme_location'    => 'footer_navigation',
			'container'         => 'div',
			'container_id'      => 'navbar-footer-menu',
//			'container_class'   => 'collapse navbar-collapse',
			'menu_class'        => 'nav',
			'depth'             => 3,
		);
		return $args;
	}

	public function primarymenu() {

		$navwalker_location = get_template_directory() . '/views/partials/class-wp-bootstrap-navwalker.php';
		if ( ! file_exists( $navwalker_location ) ) {
			// file does not exist... return an error.
			return new WP_Error( 'class-wp-bootstrap-navwalker-missing', __( 'It appears the class-wp-bootstrap-navwalker.php file may be missing.', 'wp-bootstrap-navwalker' ) );
		} else {
			// file exists... require it.
			require_once $navwalker_location;
		}
		$args = array(
			'theme_location'    => 'primary_navigation',
			'container'         => 'div',
			'container_id'      => 'navbar-primary-menu',
			'container_class'   => 'collapse navbar-collapse',
			'menu_class'        => 'navbar-nav mr-auto',
			'depth'             => 4,
			'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
			'walker'            => new \App\Controllers\WP_Bootstrap_Navwalker(),
		);
		return $args;
	}

	public function landingpagemenu() {

		$args = array(
			'theme_location'    => 'landingpage_navigation',
			'container'         => 'div',
			'container_id'      => 'navbar-top-menu',
//			'container_class'   => 'collapse navbar-collapse',
			'menu_class'        => 'nav ml-auto',
			'depth'             => 1,
		);
		return $args;
	}

    function icl_post_languages(){
        $languages = icl_get_languages('skip_missing=1');
        $language_menu = "";
        if(1 < count($languages)){
          foreach($languages as $l){
            if(!$l['active']) $langs[] = '<li><a href="'.$l['url'].'" class="language-menu-item">'.$l['language_code'].'</a></li>';
          }
          $language_menu = '<ul class="language-menu">' . join('', $langs) . '</ul>';
        }
        return $language_menu;
    }
}
