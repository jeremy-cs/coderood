<?php namespace App\Controllers;

use Sober\Controller\Controller;

class FrontPage extends Controller
{
    public function latest_posts()
    {
        $news_cat_id_nl = 11;
        $cat_id = icl_object_id($news_cat_id_nl, "category", false, ICL_LANGUAGE_CODE);
        return new \WP_Query([
            'posts_per_page'    => 5,
            'post_type'         => ['post',],
            'cat'               => $cat_id,
            'orderby'           => 'post_date',
            'order'				=> 'DESC',
            'post_status'       => 'publish'
        ]);
    }

    public function blog_posts()
    {
        $blog_cat_id_nl = 297;
        $cat_id = icl_object_id($blog_cat_id_nl, "category", false, ICL_LANGUAGE_CODE);
        return new \WP_Query([
            'posts_per_page'    => 3,
            'post_type'         => ['post',],
            'cat'               => $cat_id,
            'orderby'           => 'post_date',
            'order'				=> 'DESC',
            'post_status'       => 'publish'
        ]);
    }

	public function featured_image()
	{
		$post = get_post();
		setup_postdata($post);
		$featured_image = get_the_post_thumbnail($post->ID, 'large');
		wp_reset_postdata();
		return $featured_image;
	}

}
