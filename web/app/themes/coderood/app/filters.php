<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment', 'embed'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    collect(['get_header', 'wp_head'])->each(function ($tag) {
        ob_start();
        do_action($tag);
        $output = ob_get_clean();
        remove_all_actions($tag);
        add_action($tag, function () use ($output) {
            echo $output;
        });
    });
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Render comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );

    $data = collect(get_body_class())->reduce(function ($data, $class) use ($comments_template) {
        return apply_filters("sage/template/{$class}/data", $data, $comments_template);
    }, []);

    $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

    if ($theme_template) {
        echo template($theme_template, $data);
        return get_stylesheet_directory().'/index.php';
    }

    return $comments_template;
}, 100);

/**
 * Disable The Events Calendar Date Headers
 */
add_filter('tribe_events_list_show_date_headers', function () {
	return false;
});

add_filter('sage/display_sidebar', function () {
	static $display;

	isset($display) || $display = !in_array(true, [
		// The sidebar will NOT be displayed if any of the following return true
//		is_search(),
//		is_archive(),
//		is_category(),
		is_front_page(),
	]);

	return $display;
});

add_filter('get_search_form', function () {
	$form = '';
	echo template('partials.searchform');
	return $form;
});

//add_filter( 'style_loader_src', function ( $src ) {
//	if( strpos( $src, '?ver=' ) )
//		$src = remove_query_arg( 'ver', $src );
//	return $src;
//}, 1000, 1 );
//
//add_filter( 'script_loader_src', function ( $src ) {
//	if( strpos( $src, '?ver=' ) )
//		$src = remove_query_arg( 'ver', $src );
//	return $src;
//}, 1000, 1 );

add_action( 'pre_get_posts', function ( $query ) {
    // check for plugin using plugin name
    if ( is_plugin_active( 'sitepress-multilingual-cms/sitepress.php' ) ) {
        $news_cat_id_nl = 11;
        $cat_id = icl_object_id($news_cat_id_nl, "category", false, ICL_LANGUAGE_CODE);

        if ( $query->is_home() && $query->is_main_query() ) {
            $query->set( 'cat', $cat_id );
        }
    }
}, 10 );
